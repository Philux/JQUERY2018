'use strict';

let gulp = require('gulp');
let sass = require('gulp-sass');
let concat = require('gulp-concat');
let browserSync = require('browser-sync');
let watch = require('gulp-watch');
let uglify = require('gulp-uglify');
let pump = require('pump');
let cleanCSS = require('gulp-clean-css');
let htmlmin = require('gulp-htmlmin');

gulp.task('sass', function () {
  return gulp.src('./app/scss/global.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./app/css'));
});

gulp.task('concatJs', function() {
  return gulp.src([
    './app/javascript/jquery.js',
    './app/javascript/15-form.js'
//  './app/javascript/14-lightbox.js',
//  './app/javascript/13-slider.js',
//  './app/javascript/12-nav.js',
//  './app/javascript/11-compteur.js',
//  './app/javascript/10-accordeon.js'
//  './app/javascript/09-methode-on-and-one.js'
//  './app/javascript/08-stop-propagation.js'
//  './app/javascript/07-manip-dom.js'
//  './app/javascript/06-getters-setters.js'
//  './app/javascript/05-complete.js'
//  './app/javascript/04-animate.js'
//  './app/javascript/03-slides.js'
//  './app/javascript/02-events.js'
//  './app/javascript/01-selecteurs.js'
//  './app/javascript/*.js'
    ])
    .pipe(concat('production.js'))
    .pipe(gulp.dest('./app/js/'));
});

gulp.task('browserSync', function() {
    browserSync.init({
        server: {
            baseDir: "app"
        }
    });
});

gulp.task('watch', ['browserSync','sass','concatJs'], function(){
	gulp.watch('app/scss/**/*.scss',['sass']);
	gulp.watch('app/javascript/**/*.js',['concatJs']);
	gulp.watch('app/css/global.css', browserSync.reload);
	gulp.watch('app/*.html', browserSync.reload);
	gulp.watch('app/javascript/**/*.js', browserSync.reload);
});

// pour la prod =============================================

// generate minify js in dist folder
// https://www.npmjs.com/package/gulp-uglify
gulp.task('compress', function (cb) {
  pump([
        gulp.src('app/js/production.js'),
        uglify(),
        gulp.dest('dist/js/')
    ],
    cb
  );
});

// generate minify css in dist folder
// https://www.npmjs.com/package/gulp-clean-css
gulp.task('minify-css', () => {
  return gulp.src('app/css/global.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('dist/css/'));
});

// copy all .html in dist folder
// https://coderwall.com/p/9uzttg/simple-gulp-copy-file-task
gulp.task('copy', function(){
	gulp.src('./app/*.html')
        .pipe(gulp.dest('./dist/'));
});

// minify html in dist for prod
gulp.task('minifyHtml', function() {
  return gulp.src('app/*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('dist'));
});

gulp.task('build',['compress','minify-css','minifyHtml'], function() {});
