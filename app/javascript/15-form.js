$(function(){

    $('[data-form] [name]').blur(function(){
        // si le champ est vide
        if ($(this).val() == '') {
            // on ajoute la class 'invalid' sur le label qui suit ce champ
            // et on retire la classe 'valid'
            $(this).next().addClass('invalid').removeClass('valid');
        } else {
            // on ajoute la classe 'valid' sur le label qui suit ce champ
            // et on retire la classe 'invalid'
            $(this).next().addClass('valid').removeClass('invalid');
        }
    });
    
    $('[data-form]').submit(function(){
        let valid = true;
        $('[data-form] [required]').each(function(){
            if ($(this).val() == '') {
                valid = false;
                $(this).next().addClass('invalid').removeClass('valid');
            }
        });
        if (valid) {
            $.ajax({
                type: "post",
                url: "envoimail.php",
                data: $(this).serialize(),
                success: function (response) {
                    if (reponse == 'invalid field') {
                        // traite le cas d'erreur
                        // add class invalid sur champ mal rempli
                        // affiche texte echec
                        $('[data-form] .popup').text('champ invalide').fadeIn(); 
                    } else {
                        // traite la reponse
                        $('[data-form] [required]').each(function(){
                            ($(this).val() = '');
                        });
                        $('[data-form] .popup').text('envoi réussi').fadeIn();
                    }                    
                }
            });
        }
        return false;
    });
});