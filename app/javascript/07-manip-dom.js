//jQuery(document).ready(function({}));
//$(document).ready(function(){});
$(function(){
	// ici tout le jquery
	//	$('selecteur css').methodeJquery(...args?);
	//	$('selecteur css').event(function(...args?){
	//		$('selecteur css').methodeJquery(...args?);
	//	});

	$('button:first-of-type').click(function(){
		$('ul').before('<p>elem ajouté devant ul avec before()</p>');
	});

	$('button:nth-of-type(2)').click(function(){
		$('ul').after('<p>elem ajouté après ul avec after()</p>');
	});

	$('button:nth-of-type(3)').click(function(){
		$('ul').prepend('<li>item pas</li>');
	});

	$('button:nth-of-type(4)').click(function(){
		$('ul').append('<li>item beaucoup</li>');
	});

	let compteur = 0;
	$('button:nth-of-type(5)').click(function(){
		compteur++;
		if (compteur <= 7) {
			$('ul').append('<li>item beaucoup</li>');
		} else {
			alert('c\'est bon t\'as pas compris ?');
		}
	});

	let compt = 1;
	$('button:nth-of-type(6)').click(function(){
		compt--;
		if (compt < 0) {
			alert('c\'est bon il t\'aime PAS !');
		} else {
			$('ul').prepend('<li>item vraiment pas</li>');
		}
	});

	let i = 3;
	$('button:nth-of-type(7)').click(function(){
		i++;
		if (i <= 10) {
			$('ul').append('<li>item '+i+'</li>');
		} else {
			alert('limite atteinte');
		}
	});

	let j = 1;
	$('button:nth-of-type(8)').click(function(){
		j--;
		if (k < 0) {
			alert('limite atteinte');
		} else {
			$('ul').prepend('<li>item '+j+'</li>');
		}
	});

	// récupérer dernier caractère du dernier li
	// caster dernier caractère en number
	// assigner cette valeur à k
	let lastitem = $('li:last').text();
	let k = parseInt(lastitem[lastitem.length-1]);
	$('button:nth-of-type(9)').click(function(){
		k++;
		if (k <= 10) {
			$('ul').append('<li>item '+k+'</li>');
		} else {
			alert('limite atteinte');
		}
	});

	// récupérer dernier caractère du premier li
	// caster dernier caractère en number
	// assigner cette valeur à l
	let firstitem = $('li:first').text();
	let l = parseInt(firstitem[firstitem.length-1]);
	$('button:nth-of-type(10)').click(function(){
		l--;
		if (l < 0) {
			alert('limite atteinte');
		} else {
			$('ul').prepend('<li>item '+l+'</li>');
		}
	});

});