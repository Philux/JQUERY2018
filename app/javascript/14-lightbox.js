$(function(){

    let nbImg;
    let index;
    let src;

    // get nb images
    nbImg = $('[data-galery] img').length;

    let changeImg = function(){
        // get attr src img suivante
        src = $('[data-galery] img').eq(index).attr('src');
        // set attr src img lightbox
        $('[data-lightbox] img').attr('src', src);
        changePuce();
    }

    // generer autant de puces qu'il y en a dans la galerie
    let generatePuces = function(){
        for (i=0; i<nbImg; i++) {
            $('[data-lightbox] ul')
            .append('<li><i class="icon-circle-o"></i></li>');
        }
    }

    // change
    let changePuce = function(){
        $('[data-lightbox] i')
        .removeClass('icon-circle')
        .addClass('icon-circle-o')
        .eq(index)
        .addClass('icon-circle')
        .removeClass('icon-circle-o');
    }


    generatePuces();

    // 1.Quand on clique sur une image, on ouvre la lightbox
    $('[data-galery] img').click(function(){
        // get index img cliquée
        index = $('[data-galery] img').index($(this));
        // get attr src img cliquée
        src = $(this).attr('src');
        // set attr src img lightbox
        $('[data-lightbox] img').attr('src', src);
        // ouvre la lightbox
        $('[data-lightbox]').fadeIn().css({'display': 'flex'});
        // colorise la puce qui correspond à l'image cliquée
        changePuce();
    });

    // fermer la lightbox quand on clique sur icon-close
    $('[data-lightbox] .icon-close').click(function(){
        $('[data-lightbox]').fadeOut();
    });

    // quand on clique sur icon-angle-right
    $('[data-lightbox] .icon-angle-right').click(function(){
        index = (index+1)%nbImg;
        changeImg();
    });

    // quand on clique sur icon-angle-left
    $('[data-lightbox] .icon-angle-left').click(function(){
        index = (index-1 + nbImg)%nbImg;
        changeImg();
    });

    // 1.Quand on clique sur une puce
        // 1.1 On récupère l'index de la puce cliquée dans index
        // changeImg
    $('[data-lightbox] i').click(function(){
        index = $('[data-lightbox] i').index($(this));
        changeImg();
    })    
    
});