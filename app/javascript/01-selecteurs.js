//jQuery(document).ready(function({}));
//$(document).ready(function(){});
$(function(){
	// ici tout le jquery
//	$('selecteur css').methodeJquery(...args?);
//	$('selecteur css').event(function(...args?){
//		$('selecteur css').methodeJquery(...args?);
//	});

	$('button:first').click(function(){
		//masquer tous les p
		$('p').hide(1000);
	});

	$('button:nth-of-type(2)').click(function(){
		//afficher tous les p
		$('p').show(1000);
	});

	$('button:nth-of-type(3)').click(function(){
		//masquer 1er li de ul
		$('ul li:first-child').hide(1000);
	});

	$('button:nth-of-type(4)').click(function(){
		//masquer tous les elts qui ont un attribut href
		$('[href]').hide(1000);
	});

	$('button:nth-of-type(5)').click(function(){
		//afficher tous les elts qui ont l'attribut href
		//et la valeur http://www.orsys.fr
		$('[href="http://orsys.fr"]').show(1000);
	});

	$('button:nth-of-type(6)').click(function(){
		//afficher tous les elts qui ont l'attribut href
		//et la valeur qui contient http
		$('[href*="http"]').hide(1000);
	});

});