//jQuery(document).ready(function({}));
//$(document).ready(function(){});
$(function(){
	// ici tout le jquery
//	$('selecteur css').methodeJquery(...args?);
//	$('selecteur css').event(function(...args?){
//		$('selecteur css').methodeJquery(...args?);
//	});

	$('button:first-of-type').click(function(){
		let text = $('.text').text();
		alert (text);
	});

	$('button:nth-of-type(2)').click(function(){
		$('.text').text("Je kiffe le JQuery");
	});

	$('button:nth-of-type(3)').click(function(){
		let html = $('.html').html();
		alert (html);
	});

	$('button:nth-of-type(4)').click(function(){
		$('.html').html('<strong>Je kiffe le JQuery</strong>');
	});

	$('button:nth-of-type(5)').click(function(){
		let attr = $('img').attr('src');
		alert (attr);
	});

	$('button:nth-of-type(6)').click(function(){
		$('img').attr('src', 'img/Desert_rouge.jpg');
	});

	$('button:nth-of-type(7)').click(function(){
		let val = $('#name').val();
		alert (val);
	});

	$('button:nth-of-type(8)').click(function(){
		$('[name="name"]').val('Insert your name');
	});

});