$(function(){

	// 1.Quand on clique sur une question, on referme toutes les réponses (p)
	// 2.On ouvre la réponse juste après la question cliquée
	// 3.Mettre la classe icon-angle-down sur tous les span
	// 4.Mettre la classe icon-angle-up sur le span qui correspond à la question cliquée
	// 5.Supprimer la classe active sur tous les h1 et ajouter la classe active sur le h1 cliqué
	$('[accordeon] h1').click(function(){
		$('[accordeon] p').slideUp(100);
		// this = elt cliqué
		$(this).next().slideDown(500);
		$('[accordeon] span').attr('class', 'icon-angle-down');
		$(this).children('span').attr('class', 'icon-angle-up');
		$('[accordeon] h1').removeClass('active');
		$(this).addClass('active');
	});


});