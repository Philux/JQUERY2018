//jQuery(document).ready(function({}));
//$(document).ready(function(){});
$(function(){
	// ici tout le jquery
	//	$('selecteur css').methodeJquery(...args?);
	//	$('selecteur css').event(function(...args?){
	//		$('selecteur css').methodeJquery(...args?);
	//	});

	$('button:first-of-type').click(function(){
		$('ul').append('<li>item de plus</li>');
	});

	$('button:nth-of-type(2)').on('click',function(){
		$('ul').append('<li>item de plus</li>');
	});

	$('button:nth-of-type(3)').one('click',function(){
		$('ul').append('<li>item de plus</li>');
	});

	$(document).on('click', 'li:nth-child(4)', function(){
		$('ul').append('<li>item de plus</li>');
	});

});