$(function(){

	// 1.Au chargement de la page, définir un maxlength
	// 2.Modifier attr maxlength du textarea
	// 3.Modifier contenu texte de la balise <small>
	// 4.Dès que user effectue une saisie, on veut compter le nombre de caractères
	// 5.On veut soustraire ce nombre à max
	
	let max = 100;
	let lgChaine;
	let pastedData;
	$('[compteur] textarea').attr('maxlength', max);
	$('[compteur] small').text(max);
	$('[compteur] textarea').keyup(function(){
		lgChaine = $(this).val().length;
		$('[compteur] small').text(max - lgChaine);
	});
	$('[compteur] textarea').bind('paste', function(e){
		pastedData = e.originalEvent.clipboardData.getData('text');
		lgChaine = pastedData.length + $(this).val().length;
		if (lgChaine < max) {
			$('[compteur] small').text(max - lgChaine);
		} else {
			$('[compteur] small').text(0);
		}
	});

});