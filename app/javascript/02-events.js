//jQuery(document).ready(function({}));
//$(document).ready(function(){});
$(function(){
	// ici tout le jquery
//	$('selecteur css').methodeJquery(...args?);
//	$('selecteur css').event(function(...args?){
//		$('selecteur css').methodeJquery(...args?);
//	});

	$('button:first-of-type').click(function(){
		//modifie le premier p
		$('p:first-of-type').text('whaouh trop fort Jquery');
	});

	$('button:nth-of-type(2)').dblclick(function(){
		//modifie le premier p sur un double click
		$('p:first-of-type').text('whaouh un double click');
	});

	$('button:nth-of-type(3)').mouseenter(function(){
		//mouse enter pour modifier le texte du premier p
		$('p:first-of-type').text('je kiffe les mouse enter !');
	});

	$('button:nth-of-type(4)').mouseleave(function(){
		//mouse leave pour modifier le texte du premier p
		$('p:first-of-type').text('mouse leave works !');
	});

	$('button:nth-of-type(5)').hover(function(){
		//hover pour modifier le texte du premier p
		$('p:first-of-type').text('mouse enter works !');
	}, function(){
		$('p:first-of-type').text('mouse leave works !');

	});

	$('input').focus(function(){
		//change le contenu du champ quand on entre sur le champ
		$(this).val('Ce champ prend le focus');
	});

	$('input').blur(function(){
		//change le contenu du champ quanbd on quiite le champ
		$(this).val('Ce champ perd le focus');
	});

	$(window).keyup(function(){
		//modifie le premier champ lorsqu'on relâche une touche
		$('p:first-of-type').text('keyup works !');
	});


});