//jQuery(document).ready(function({}));
//$(document).ready(function(){});
$(function(){
	// ici tout le jquery
//	$('selecteur css').methodeJquery(...args?);
//	$('selecteur css').event(function(...args?){
//		$('selecteur css').methodeJquery(...args?);
//	});

	$('button:first-of-type').click(function(){
		//positionner elt en left 500 anim dure 1sec
		$('.blue').animate({'left': 500}, 1000);
		$('.red').animate({'left': 500}, 800);
		$('.green').animate({'left': 500}, 1200);
	});

	$('button:nth-of-type(2)').click(function(){
		//animate on multiple css properties
		$('.blue').animate({
			'left': 500,
			'top': 500,
			'width': '200px',
			'height': '200px'
		}, 1000);
		$('.red').animate({'left': 500}, 800);
		$('.green').animate({'left': 500}, 1200);
	});

	$('button:nth-of-type(3)').click(function(){
		//animate avec chainage
		$('.blue').animate({
			'left': 500,
			'top': 500,
			'width': '200px',
			'height': '200px'
		}, 1000)
		.animate({
			'left': 0,
			'top': 0,
			'width': '100px',
			'height': '100px'
		}, 200);
	});


	// prendre un block au hasard, chainer plusieurs anination et le faire revenir à sa place
	$('button:nth-of-type(4)').click(function(){
		let e = $('.red');
		e.animate({
			'left': 300,
			'top': 100
		}, 1200)
		.animate({
			'left': 1000,
			'top': 200
		}, 800)
		.animate({
			'left': 500,
			'top': 100
		}, 1500)
		.animate({
			'left': 0,
			'top': 0
		}, 100);
	});


	$('button:nth-of-type(5)').click(function(){
		//reset
		$('div').animate({
			'left': 0,
			'top': 0,
			'width': '100px',
			'height': '100px'
		}, 200);
	});


});