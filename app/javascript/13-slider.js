$(function(){
	// Au chargement de la page, on autorise le user à cliquer sur un bouton
	// pour défiler en left ou en right
	let acceptDefil=true;

	// Récupérer largeur du slider pour l'affecter aux li
	let l = $('[data-slider]').width();
	$('[data-slider] li').width(l);

	// 1.Quand on resize la fenetre
		// 1.1 recalculer la largeur du slider
		// 1.2 ré-affecter cette largeur aux li
	$(window).resize(function(){
		l = $('[data-slider]').width();
		$('[data-slider] li').width(l);
	});

	// barre de progression
	let animation = function(){
		$('[data-slider] .progress').animate({'width': '100%'}, 2000, function(){
			$(this).css({'width': '0%'});
		});
	}

	// 1.crééer une fonction pour : animer ul en left -200
	// 2.une fois animation terminée :
		// 2.1.mettre le premier li après le dernier li
		// 2.2.repositionner le ul en left 0
	let defilRight = ()=> {
		$('[data-slider] ul').animate({'left': -l}, 1000, function(){
			$('[data-slider] li:last').after( $('[data-slider] li:first') );
			$(this).css({'left': 0});
			acceptDefil=true;
			animation();
		});
	};

	// 1.Positionner ul en left -200 sans animation en utilisant la methode css
	// 2.Dans le même temps, mettre dernier li devant le premier li
	// 3.Animer ul de left -? à left 0
	let defilLeft = ()=> {
		$('[data-slider] ul').css({'left': -l});
		$('[data-slider] li:first').before( $('[data-slider] li:last') );
		$('[data-slider] ul').animate({'left': 0}, 1000, function(){
			acceptDefil=true;
			animation();
		});
	};

	// 1.Quand on clique sur le bouton left
		// 1.1 stopper le défilement auto
		// 1.2 appeler une fois defilLeft
		// 1.3 on accepte le défilement si anim terminée 
	$('[data-slider] .icon-angle-left').click(function() {
		if (acceptDefil) {
			acceptDefil = false;			
			clearInterval(interval);
			defilLeft();
		}
	});

	// 1.Quand on clique sur le bouton right
		// 1.1 stopper le défilement auto
		// 1.2 appeler une fois defilRight 
	$('[data-slider] .icon-angle-right').click(function() {
		if (acceptDefil) {
			acceptDefil = false;			
			clearInterval(interval);
			defilRight();
		}

	});
	
	let interval = setInterval(defilRight, 3000);

});